package pl.edu.pwsztar;

import java.util.Arrays;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return this.id.length() == 11;
    }

    @Override
    public Optional<Sex> getSex(){
        if(this.id.trim().length() < 10){
            return Optional.empty();
        }
        return this.id.charAt(9) % 2 == 0 ? Optional.of(Sex.WOMAN) : Optional.of(Sex.MAN);
    }

    @Override
    public boolean isCorrect() {
        return isCorrectSize() && checkPeselSum();
    }

    @Override
    public Optional<String> getDate() {
        if(!isCorrectSize()){
            return Optional.empty();
        }
        return Optional.of(getCorrectDate(getCorrectCeuntry()));
    }

    private boolean checkPeselSum() {
        int weights[] = { 9 ,7 ,3 ,1 ,9 ,7 ,3 ,1 ,9 ,7 };
        int sum = 0;
        int[] peselArray = getIdAsIntArray();
        for (int i = 0; i < weights.length; i++) {
            sum += weights[i] * peselArray[i];
        }
        return sum % 10 == peselArray[10];
    }

    private int getCorrectCeuntry(){
        int[] pesel = getIdAsIntArray();
        int month = (pesel[2]*10) + pesel[3];
        if(month >= 81 &&  month <= 92){
            return 80;
        }
        else if(month >= 21 && month <= 32){
            return 20;
        }
        else if(month >= 41 && month <= 52){
            return 40;
        }
        else if(month >= 61 && month <= 72){
            return 60;
        }
        return -1;
    }

    private String getCorrectDate(int ceuntry){
        int[] pesel = getIdAsIntArray();
        String month = id.charAt(2) + "" + id.charAt(3);
        if(ceuntry != -1){
            int x = (pesel[2]*10) + pesel[3] - ceuntry;
            if(x<10){
                month = "0"+x;
            }
            else{
                month = String.valueOf(x);
            }
        }
        if(ceuntry == 80){
            return pesel[4]+""+pesel[5]+"-"+month+"-"+"18"+pesel[0]+pesel[1];
        }
        else if(ceuntry == 20){
            return pesel[4]+""+pesel[5]+"-"+month+"-"+"20"+pesel[0]+pesel[1];
        }
        else if(ceuntry == 40){
            return pesel[4]+""+pesel[5]+"-"+month+"-"+"21"+pesel[0]+pesel[1];
        }
        else if(ceuntry == 60){
            return pesel[4]+""+pesel[5]+"-"+month+"-"+"22"+pesel[0]+pesel[1];
        }
        return pesel[4]+""+pesel[5]+"-"+month+"-"+"19"+pesel[0]+pesel[1];
    }

    private String[] getIdStringArray(){
        return id.split("");
    }

    private int[] getIdAsIntArray(){
        return Arrays.stream(getIdStringArray()).mapToInt(Integer::parseInt).toArray();
    }
}
