package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll
import static pl.edu.pwsztar.UserIdChecker.*
import static pl.edu.pwsztar.UserIdChecker.Sex.*
class UserIdSpec extends Specification {


    @Unroll
    def "should check pesel #pesel length"() {
        given: "initial data"
            def user = new UserId(pesel)
        when: "check pesel length"
            def result = user.isCorrectSize()
        then:"gets result"
            expected == result
        where:
            pesel      |   expected
        "01060435869"  |     true
        "54011301661"  |     true
        "87010943000"  |     true
        "0010294247"   |     false
        "7101024"      |     false
        ""             |     false
    }

    @Unroll
    def "should get proper sex"(){
        given: "initial data"
            def user = new UserId(pesel)
        when: "check user sex"
            def result = user.getSex()
        then: "get result"
            expected == result.get()
        where:
            pesel      |   expected
        "40101867993"  |     MAN
        "69070931526"  |     WOMAN
        "00320243710"  |     MAN
        "23030133971"  |     MAN
        "37040363521"  |     WOMAN
        "40022161347"  |     WOMAN
    }

    @Unroll
    def "should check if pesel is correct"(){
        given:"initial data"
            def user = new UserId(pesel)
        when: "check pesel"
            def result = user.isCorrect()
        then: "get result"
            expected == result
        where:
            pesel      |   expected
        "72141411358"  |    false
        "55080518162"  |    false
        "00320243710"  |    true
        "23030133971"  |    true
        "37040363521"  |    true
        "40022161347"  |    true
    }

    @Unroll
    def "should get date from pesel #pesel"(){
        given:"initial data"
            def user = new UserId(pesel)
        when: "get date"
            def result = user.getDate()
        then: "get result"
            expected == result.get()
        where:
            pesel      |   expected
        "00320243710"  |    "02-12-2000"
        "23030133971"  |    "01-03-1923"
        "37040363521"  |    "03-04-1937"
        "40022161347"  |    "21-02-1940"
    }
}
